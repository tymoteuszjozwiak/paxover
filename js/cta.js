document.addEventListener("DOMContentLoaded", function() {
    const cta = document.querySelector(".cta");
    const nav = document.querySelector("nav");

    if (cta && nav) {
        setTimeout(() => {
            cta.style.transition = "height 0.5s ease, opacity 0.5s ease";
            cta.style.height = "0";
            cta.style.opacity = "0";

            setTimeout(() => {
                nav.style.setProperty("margin-top", "1%", "important");
            }, 500);
        }, 10000);
    }
});
