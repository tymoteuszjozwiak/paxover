# Paxover

Grav CMS theme for my personal website

## License info

Paxover is not basically designed for public use (I developed it for my personal needs), but it's not prohibited. You must be aware that Paxover doesn't work like popular Grav themes, where you can configure everything at settings; some pages are created rigidly in templates (of course, you can modify them). It's distributed under the MIT license. [Full text](https://codeberg.org/tymoteuszjozwiak/paxover/src/branch/master/LICENSE).

## Installation

GPM installation is not supported. To install Paxover, [download the zip version of this repository](https://codeberg.org/tymoteuszjozwiak/paxover/archive/master.zip) and unzip it under `/your-site-path/user/themes`. Then, rename the folder to `paxover`. You'll need only to enable Paxover in your Admin Panel settings or change enabled value to `true` under `/your-site-path/user/config/themes/paxover.yaml`.

## Custom images

Paxover images are placed in the images folder. You can replace them, but you need to preserve the file names.

- `favicon-16x16.png`: 16x16 favicon for your site
- `favicon-32x32.png`: 32x32 favicon for your site
- `logo.png`: your site's logo, which appears as a meta og:image on non-post pages (for example, Default and Index).

## Supported templates

- Default view template (Default)
- Main page view template (Index)
- Error page view template (Error)
- Blog listing view template (Blog)
- Blog post view template (Post)

## Translations

Paxover is designed to use default language (configured in Grav settings) on all the pages, with additional English translation for non-blog pages. You can configure only the majority of button and link titles in the theme settings (keep in mind that Paxover is a custom theme for my personal needs).
